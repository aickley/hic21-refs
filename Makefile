BEDTOOLS=/software/UHTS/Analysis/BEDTools/2.26.0/bin/bedtools
BOWTIE2_BUILD=/software/UHTS/Aligner/bowtie2/2.3.1/bin/bowtie2-build
SNPS=/scratch/ug/monthly/ikolpako/hic21/snps/

all: sequences

# Download hg19 reference (main chromosomes)
hg19/hg19.fa:
	cd hg19 && make hg19.fa

# Use the SNPs of the normal twin
twins/hg19.fa: ${SNPS}/twins/TwN/snps.vcf hg19/hg19.fa ${BEDTOOLS}
	mkdir -p $(@D)
	${BEDTOOLS} maskfasta -fi hg19/hg19.fa -fo $@ -bed $<

unrel/hg19.fa: hg19/hg19.fa
	mkdir -p $(@D)
	ln $< $@

%/hg19.fa.bt2: ref/%/hg19.fa ${BOWTIE2_BUILD}
	rm -rf $@
	bsub -I -M 20000000 -n 16 -e $@.err -o $@.out ${BOWTIE2_BUILD} --threads 16 $< $<

sequences: twins/hg19.fa unrel/hg19.fa

indices: twins/hg19.fa.bt2 unrel/hg19.fa.bt2

