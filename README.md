# hic21-refs

Generating bowtie2 indices for hg19 and hg19 with twins' SNPS.
(part of [hic21](https://bitbucket.org/aickley/hic21-meta/) pipeline).

## Usage 

Running
```
make sequences
```
will create reference genomes in `./twins` and `./unrel` respectively.

Running
```
make indices
```
will create bowtie2 indexes for both reference (running each indexing
jobs via LSF with 16 threads each).

## Dependencies

### Data

  *  hg19 reference: dowloaded automatically
  *  SNPs for the twins: from `/scratch/ug/monthly/ikolpako/hic21/snps/` 

### Software

  *  [bowtie2](http://bowtie-bio.sourceforge.net/bowtie2/index.shtml): v2.26.0 (from `/software`)
  *  [bedtools](https://bedtools.readthedocs.io/en/latest/): v2.3.1 (from `/software`)

